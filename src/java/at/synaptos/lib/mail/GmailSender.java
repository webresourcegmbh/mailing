/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.synaptos.lib.mail;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.Objects;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Daniel Moertenschlag
 */
public class GmailSender extends MailSender {

    private final String refreshToken;
    private String oAuthToken;
    private final String email;
    private final String url;

    public GmailSender(String url, String email, String oAuthToken) {
        Objects.requireNonNull(oAuthToken);
        Objects.requireNonNull(email);
        Objects.requireNonNull(url);
        this.refreshToken = oAuthToken;
        this.url = url;
        this.email = email;
    }

    @Override
    protected Authenticator init() {
        this.oAuthToken = renewToken();
        props.setProperty(SENDER_MAIL, this.email);
        return null;
    }

    /*
    Renew access token if expired
     */
    private String renewToken() {

        try {
            GoogleRefreshTokenRequest request = new GoogleRefreshTokenRequest(new NetHttpTransport(), new JacksonFactory(),
                    refreshToken, GMailConstants.GMAIL_CLIENT_ID, GMailConstants.GMAIL_CLIENT_SECRET);
            GoogleTokenResponse respo = request.execute();
            return respo.getAccessToken();
        } catch (Exception e) {
            throw new MailerException(MailerException.MailError.AUTHENTICATION_FAILED, e.getMessage(), e);
        }
    }

    Credential convertToGoogleCredential(String accessToken, String refreshToken, String apiSecret, String apiKey) {
        HttpTransport httpTransport = new NetHttpTransport();
        JsonFactory jsonFactory = new JacksonFactory();
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(jsonFactory).setClientSecrets(apiKey, apiSecret).build();
        credential.setAccessToken(accessToken);
        credential.setRefreshToken(refreshToken);

        return credential;
    }

    @Override
    protected void transport(MimeMessage msg, Session session) throws Exception {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
        Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                convertToGoogleCredential(this.oAuthToken, this.refreshToken, GMailConstants.GMAIL_CLIENT_SECRET,
                        GMailConstants.GMAIL_CLIENT_ID))
                .build();

        service.users().messages().send("me", createMessageWithEmail(msg)).execute();
    }

    private Message createMessageWithEmail(MimeMessage emailContent) throws IOException, MessagingException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        emailContent.writeTo(buffer);
        byte[] bytes = buffer.toByteArray();
        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
        Message message = new Message();
        message.setRaw(encodedEmail);
        return message;
    }

}
