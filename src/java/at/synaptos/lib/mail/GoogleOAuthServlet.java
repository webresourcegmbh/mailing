/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.synaptos.lib.mail;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.extensions.servlet.auth.oauth2.AbstractAuthorizationCodeServlet;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Strings;
import java.io.IOException;
import java.util.Collections;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Dieses Servlet übernimmt die Kommunikation mit google, dieses muss daher auch bei Google in den Anmeldendaten hinterlegt sein.
 * OAuth authorization servlet zum empfangen der Nachrichten von Google.
 *
 * @see https://developers.google.com/identity/protocols/OAuth2
 * @see https://developers.google.com/identity/protocols/OAuth2WebServer
 *
 * create a google developer account using
 * https://console.developers.google.com/apis/credentials //
 * 1046182421895-qid4gp7ok2ifir10f9dmna666gcbu3dr.apps.googleusercontent.com //
 * FPiPC2RTR8Ti0sRmhMphOcSe
 *
 *
 * Client ID
 * 1046182421895-qid4gp7ok2ifir10f9dmna666gcbu3dr.apps.googleusercontent.com
 * Client Secret FPiPC2RTR8Ti0sRmhMphOcSe info
 *
 * https://developers.google.com/identity/sign-in/web/
 * @author Daniel Moertenschlag
 */
public class GoogleOAuthServlet extends AbstractAuthorizationCodeServlet {

    protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        if (Strings.isNullOrEmpty(req.getParameter("code"))) {
            super.service(req,  resp);
            return;
        }
        final String code = req.getParameter("code");
        
        try {
            GoogleAuthorizationCodeTokenRequest request
                    = new GoogleAuthorizationCodeTokenRequest(new NetHttpTransport(), new JacksonFactory(),
                            GMailConstants.GMAIL_CLIENT_ID,
                            GMailConstants.GMAIL_CLIENT_SECRET,
                            code,getRedirectUri(req));
            request.setScopes(Collections.singleton(GMailConstants.GMAIL_MAIL_SEND));
            request.setGrantType("authorization_code");
            GoogleTokenResponse response = request.execute();
            String rToken = response.getRefreshToken();
            /*
            das token kann auch daherhaft in der Datenbank pro Benutzer gespeichert werden.
            */
            req.getSession().setAttribute("tokenAccess", rToken != null ? rToken : response.getAccessToken());
        } catch (Exception e) {
            throw new MailerException(MailerException.MailError.UNKNOWN, e.getMessage(), e);
        }
        resp.sendRedirect(req.getContextPath()); // begin from start but with a safed token
    }
    
    @Override
    protected String getRedirectUri(HttpServletRequest req) throws ServletException, IOException {
        GenericUrl url = new GenericUrl(req.getRequestURL().toString());
        url.setRawPath(req.getContextPath()+"/goauth");
        return url.build();
    }

    @Override
    protected AuthorizationCodeFlow initializeFlow() throws IOException {
        return new GoogleAuthorizationCodeFlow.Builder(
                new NetHttpTransport(), JacksonFactory.getDefaultInstance(),
                GMailConstants.GMAIL_CLIENT_ID, GMailConstants.GMAIL_CLIENT_SECRET,
                Collections.singleton(GMailConstants.GMAIL_MAIL_SEND))
                .setApprovalPrompt("force")
                .setAccessType("offline").build();
    }

    @Override
    protected String getUserId(HttpServletRequest req) throws ServletException, IOException {
        // return user ID
        return "";
    }
}
