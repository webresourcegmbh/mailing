/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.synaptos.lib.mail;

/**
 *
 * @author Daniel Moertenschlag
 */
public class MailerException extends RuntimeException {

    public static enum MailError {
        INVALID_MAIL,
        SEND_FAILED,
        AUTHENTICATION_FAILED,
        UNKNOWN
    }
    
    private MailError type;

    public MailerException(MailError type, String message) {
        super(message);
        this.type = type;
    }

    public MailerException(MailError type, String message, Throwable cause) {
        super(message, cause);
        this.type = type;
    }

    public MailerException(MailError type, Throwable cause) {
        super(cause);
        this.type = type;
    }

    public MailError getType() {
        return type;
    }
}
