/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.synaptos.lib.mail;

import java.util.Date;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/**
 *
 * @author Daniel Moertenschlag
 */
public abstract class MailSender {

    protected static final Logger LOGGER = Logger.getLogger(MailSender.class.getCanonicalName());

    protected static final String SENDER_MAIL = "SENDER_MAIL";
    protected static final String REPLY_MAIL = "REPLY_MAIL";
    protected static final String MAIL_ENCODING = "MAIL_ENCODING";
    protected static final String SMTP_HOST = "mail.smtp.host";
    protected static final String SMTP_PORT = "mail.smtp.port";
    protected static final String SMTP_AUTH = "mail.smtp.auth";
    protected static final String SMTP_AUTH_PORT = "mail.smtp.socketFactory.port";
    protected static final String SMTP_AUTH_CLASS = "mail.smtp.socketFactory.class";
    protected Properties props = new Properties();
    protected boolean html = false;

    abstract Authenticator init();

    protected Session establishConnection() {
        SecurityManager security = System.getSecurityManager();
        if (security == null) {
            return Session.getInstance(props, init());
        }
        return Session.getDefaultInstance(props, init());
    }

    public void send(String subject, String content,
            Collection<String> receiver) {
        this.send(subject, content, receiver, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
    }

    public void send(String subject, String content,
            Collection<String> receiver,
            Collection<String> cc) {
        Objects.requireNonNull(receiver);
        this.send(subject, content, receiver, cc, new ArrayList<>(), new ArrayList<>());
    }

    public void send(String subject, String content,
            Collection<String> receiver,
            Collection<String> cc,
            Collection<String> bcc) {
        Objects.requireNonNull(receiver);
        this.send(subject, content, receiver, cc, bcc, new ArrayList<>());
    }

    public void send(String subject, String content,
            Collection<String> receiver,
            Collection<String> cc,
            Collection<String> bcc,
            Collection<MailAttachment> attachments) {
        Objects.requireNonNull(receiver);
        Session session = null;
        try {
            final MimeMessage msg = new MimeMessage(session = establishConnection());
            //set message headers
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress(props.getProperty(SENDER_MAIL)));

            if (props.containsKey(REPLY_MAIL)) {
                msg.setReplyTo(InternetAddress.parse(props.getProperty(REPLY_MAIL)));
            }
            msg.setSubject(subject, props.getProperty(MAIL_ENCODING, "UTF-8"));

            if (attachments.size() == 0 && !this.isHtml()) {
                msg.setText(content, "UTF-8");
            } else {
                // Create the message part
                final BodyPart messageBodyPart = new MimeBodyPart();
                // Now set the actual message
                messageBodyPart.setText(content);
                // Create a multipar message
                final Multipart multipart = new MimeMultipart();
                // Set text message part
                multipart.addBodyPart(messageBodyPart);

                attachments.forEach(attachment -> {
                    try {
                        final BodyPart messageBodyPart1 = new MimeBodyPart();
                        
                        messageBodyPart1.setFileName(attachment.getName());
                        ByteArrayDataSource ds = new ByteArrayDataSource(attachment.getContent(), attachment.getType());
                        messageBodyPart1.setDataHandler(new DataHandler(ds));
                        multipart.addBodyPart(messageBodyPart1);
                    } catch (MessagingException ex) {
                        Logger.getLogger(MailSender.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });

                msg.setContent(multipart);
            }

            msg.setSentDate(new Date());

            receiver.stream().forEach(recv
                    -> {
                try {
                    msg.addRecipients(Message.RecipientType.TO,
                            InternetAddress.parse(recv, false));
                } catch (MessagingException mEx) {
                    throw createMailingException(mEx);
                }
            });

            receiver.stream().forEach(recv
                    -> {
                try {
                    msg.addRecipients(Message.RecipientType.TO,
                            InternetAddress.parse(recv, false));
                } catch (MessagingException mEx) {
                    throw new MailerException(MailerException.MailError.INVALID_MAIL, mEx.getMessage(), mEx);
                }
            });

            LOGGER.fine("Message is ready");

            transport(msg, session);
            LOGGER.fine("EMail Sent Successfully!!");

        } catch (MessagingException mEx) {
            throw createMailingException(mEx);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Failed to send mail", e);
            throw new MailerException(MailerException.MailError.UNKNOWN, e);
        }
    }

    protected void transport(MimeMessage msg, Session session) throws Exception {
        Transport transport = session.getTransport();
        transport.connect();
        transport.sendMessage(msg, msg.getAllRecipients());
        transport.close();
    }

    public Properties getProps() {
        return props;
    }

    public boolean isHtml() {
        return html;
    }

    public void setHtml(boolean html) {
        this.html = html;
    }

    protected MailerException createMailingException(MessagingException mEx) {
        if (mEx instanceof javax.mail.AuthenticationFailedException) {
            LOGGER.log(Level.INFO, "Failed to authenticate", mEx);
            return new MailerException(MailerException.MailError.AUTHENTICATION_FAILED, mEx);
        }
        return new MailerException(MailerException.MailError.UNKNOWN, mEx);
    }
}
