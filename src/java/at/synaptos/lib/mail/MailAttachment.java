/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.synaptos.lib.mail;

import javax.activation.MimeType;

/**
 * attachment data type, including content, file type and attachment name
 * @author Daniel Moertenschlag
 */
public class MailAttachment {
    /**
     * mime types see here: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
     */
    public static final String MIME_TYPE_TEXT = "text/plain";
    public static final String MIME_TYPE_HTML = "text/html";
    public static final String MIME_TYPE_PDF  = "application/pdf";
    private byte[] content;
    private String type;
    private String name;
    
    public MailAttachment() {
        
    }

    public MailAttachment(byte[] content, String type, String name) {
        this.content = content;
        this.type = type;
        this.name = name;
    }
    

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
