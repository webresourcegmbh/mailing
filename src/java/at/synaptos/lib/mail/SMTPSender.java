/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.synaptos.lib.mail;

import java.util.Objects;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 *
 * @author Daniel Moertenschlag
 */
public class SMTPSender extends MailSender {

    private String password, userName, hostName;
    private int port;
    private boolean secure;

    public SMTPSender(String userName,String password, 
            String hostName,
            int port,
            boolean secure) {
        Objects.requireNonNull(password);
        Objects.requireNonNull(userName);
        Objects.requireNonNull(hostName);
        this.password = password;
        this.userName = userName;
        this.hostName = hostName;
        this.port = port;
        this.secure = secure;
    }

    public SMTPSender() {

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isSecure() {
        return secure;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    @Override
    protected Authenticator init() {
        props.put(SENDER_MAIL, userName);
        props.setProperty("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", hostName);
        props.put("mail.smtp.port", port);
        if (secure) {
            props.put("mail.smtp.starttls.enable", "true"); //TLS
        }
        if (this.password == null) {
            return null;
        }
        props.put("mail.smtp.auth", "true");
        return new SMTPAuthenticator();
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(userName, password);
        }
    }
}
