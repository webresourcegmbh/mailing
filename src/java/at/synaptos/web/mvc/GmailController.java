/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.synaptos.web.mvc;

import at.synaptos.lib.mail.GmailSender;
import at.synaptos.lib.mail.MailAttachment;
import at.synaptos.lib.mail.SMTPSender;
import java.util.Collections;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * bitte beim controller, zwingend die richtigen Pfade einstellen, muss dem Eintrag im web.xml entsprechen.
 * Das Token, das hier in der Sitzung gespeichert wird, kann auch in der Datenbank gespeichert werden, d.h. auch dauerhaft.
 * @author Daniel Moertenschlag
 */
@ManagedBean(name = "gmailController")
@RequestScoped()
public class GmailController {

    @ManagedProperty(name = "target", value = "")
    private String target;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String sendGmail() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);

        GmailSender gmailSender = new GmailSender(
                "http://localhost:8080"+facesContext.getExternalContext().getRequestContextPath()+"/goauth",
                "daniel@5lvlup.com",
                (String)session.getAttribute("tokenAccess"));
        gmailSender.setHtml(false);
        gmailSender.send("Testnachricht", "Das ist eine Testnachricht", Collections.singleton(this.target),
                Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.singleton(new MailAttachment("hallo anhang".getBytes(), "text/plain", "anhang.txt")));
        return "success";
    }
}
