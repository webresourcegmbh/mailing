/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.synaptos.web.mvc;

import at.synaptos.lib.mail.SMTPSender;
import java.io.Serializable;
import java.util.Collections;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Daniel Moertenschlag
 */
@ManagedBean(name = "sendController")
@RequestScoped()
public class SendController implements Serializable {

    @ManagedProperty(name = "userName", value = "")
    private String userName;
    @ManagedProperty(name = "password", value = "")
    private String password;
    @ManagedProperty(name = "target", value = "")
    private String target;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String sendMail() {
        SMTPSender gmailSender = new SMTPSender(this.userName,
                this.password,
                "smtp.gmail.com",
                587,
                true);
        gmailSender.setHtml(false);
        gmailSender.send("Testnachricht", "Das ist eine Testnachricht" , Collections.singleton(this.target));
        return "success";
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
