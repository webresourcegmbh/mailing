# Zum Projekt

beim Projekt handelt es sich um vollständigen Netbeans Projekt, das eine Webanwendung auf Basis von JSF enthält.
Diese Projekt lässt sich auch mit anderen IDE's importieren, Netbeans ist daher nicht zwingend notwendig. Zum Importieren
einfach Ihre üblichen Create Project with Existing Sources Menüpunkte verwenden.

## Einstellungen für Ihre Anwendung
In den GMailConstants.java müssen Sie Ihre Anwendungsparameter laut Google Developer Console hinterlegen, Details dazu weiter unten.
Außerdem müssen Sie in Ihrer web.xml das Google Authentication Servlet hinterlegen. Der Entsprechende Pfad muss auch im GMailController.java eingetragen werden.

## Lib Verzeichnis
Im Lib Verzeichnis finden Sie alle notwendigen JAR's, evtl. haben Sie bereits die notwendigen JAR's in Ihrem Applikationsserver, dann sind diese nicht mehr zu integrieren.

# Mailing mit SMTP und Google GMail API

Folgende Schritte sind notwendig für ein einen erfolgen Mailversand, speziell via Google GMail API.

# Google Console anlegen
Um via GMail API Mails zu versenden ist eine **Google App Registrierung** zwingend erforderlich.
Diese kann via der Google Developer Console kostenlos angelegt werden. Diese finden Sie unter
https://console.developers.google.com/?hl=de.

## Projekt anlegen
Zuerst müssen Sie dort ein neues Projekt anlegen. Einfach hier einen Projektnamen vergeben, sinnvoll
wäre Beispielsweise Synaptos. Evtl. müssen vorab eine Organisation registrieren - hier einfach ihre
Geschäftsdaten angeben.

## GMail API aktivieren

Für dieses Projekt können Sie nun die notwendigen API's aktivieren. Dazu den Button   
APIS UND DIENSTE AKTIVIEREN verwenden und Gmail API aktivieren. Die anderen API's werden
hier nicht verwendet.

## Anmeldedaten erstellen
Im nächsten Schritt können Sie mit Anmeldedaten erstellen die API Keys für Ihre Anwendung herunterladen.
Diese Daten müssen geheim bleiben, da man ansonsten Ihre API verwenden könnte. Wichtig der Name
der API wird dem Kunden auch deutlich angezeigt, die Eingabe sollte daher sinnvoll sein - also z.B. Synaptos KG.

### Autorisierte Weiterleitungs-URIs
Die hier eingetragenen URL's müssen ihrem Zielurls (also den Google Auth Servlet) entsprechen. Sinnvoll ist
hier auch localhost einzutragen, damit eine lokale Entwicklung unterstützt. Achtung: die hier eingetragenen
URL's müssen exakt mit den Ziel URLs zusammenpassen und müssen auch in der Anwendung wieder
so verwendet werden.

### OAuth-Zustimmungsbildschirm
Danach können Sie die OAuth Zustimmung konfigurieren die Ihr Kunde sehen soll.  Auch hier sinnvolle
Eingaben verwenden, da der Kunde diese sieht. Zwingend erforderlich ist es außerdem den Bereich
um auth/gmail.send zu ergänzen und Mail senden zu dürfen. Sie können hier auch weitere Berechtigungen
vom Kunden einfordern, die diese aber dann alle Bestätigen muss und auch klar angezeigt bekommt.
Für die lokale Entwicklung empfiehlt sich der Typ intern, da hier keine Überprüfung durch google erfolgt.
Bei **Öffentlich** erfolgt eine Überprüfung durch google - dieses **muss für den Produktiveinsatz erfolgen**.